# Email Viewer

A simple PHP .eml file viewer

## Support
For info or bugs please email or setup a bug ticket.

## Roadmap
There is an end goal, to get the project done...

## Contributing
- 4 Spaces, not tabs
- Keep it clean
- Try not to go more than 80 charater width
- No magic quotes (They're so slow!)
- Enjoy

## Authors and acknowledgment
Sn0wlink - David Collins-Cubitt

## License
GPL Licence

## Project status
Currently under development