<?php 

/*
	Name: Email Viewer
	Author: David Collins-Cubitt
	Date: July 2022
	Description: To unpack and preview 'eml' files in a web browser.
*/

// Enable Debug
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

// Include Librarys
include 'email-viewer.php';

// Start output display
echo '<h1><center>Fred\'s Email Processor</center></h1>';

// Load Test
email_viewer('test-email.eml');

// EOF