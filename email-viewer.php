<?php 

/*
    Name: Email Viewer
    Author: David Collins-Cubitt
    Date: July 2022
    Description: To unpack and preview 'eml' files in a web browser.
*/

function emaiL_viewer($Input_File) {

    // Config
    $DEBUG = True; // Setup debug (boolean)

    // Load the data
    $Loaded_File = file_get_contents($Input_File);

    // Print debug information.
    if ($DEBUG) {
        if ($Loaded_File != '') {echo 'Debug: File Loaded<br />';}
    }

    // Unpack File
    $Unpacked_File = explode(PHP_EOL, $Loaded_File);

    // Start line identification
    foreach ($Unpacked_File as $Message_Line) {

        // Detect -> Return Path:
        $Line_Identifier = substr($Message_Line, 0, 12);
        if ($Line_Identifier == 'Return-Path:') {
            $Message_Line = str_replace('Return-Path:', '', $Message_Line);
            $Message_Line = str_replace('<', '', $Message_Line);
            $Message_Line = str_replace('>', '', $Message_Line);
            $Message_Line = str_replace(' ', '', $Message_Line);
            $Message_Line = str_replace(',', '', $Message_Line);
            $Return_Path = $Message_Line;
        }

        // BUGBUG -> This does not handle Names yet... Grrrrr
        
        // Detect -> Message To:
        $Line_Identifier = substr($Message_Line, 0, 3);
        if ($Line_Identifier == 'To:') {
            $Message_Line = str_replace('To:', '', $Message_Line);
            $Message_Line = str_replace('<', '', $Message_Line);
            $Message_Line = str_replace('>', '', $Message_Line);
            $Message_Line = str_replace(' ', '', $Message_Line);
            $Message_Line = str_replace(',', '', $Message_Line);
            $Message_To = $Message_Line;
        }
    }

    // Debug Info
    if ($DEBUG) {
        $Debug_Information = '<br />';
        $Debug_Information .= 'Return Path: ' . $Return_Path . '<br />';
        $Debug_Information .= 'Message To: ' . $Message_To . '<br />';

        // Echo or return, It's up to you!
        echo $Debug_Information;
    }
    
}

// EOF